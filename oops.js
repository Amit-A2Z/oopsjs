"use strict";

/* 
What is this?
In JavaScript, the this keyword refers to an object.
The this keyword refers to different objects depending on how it is used:
In an object method, this refers to the object.
Alone, this refers to the global object.
In a function, this refers to the global object.
In a function, in strict mode, this is undefined.
In an event, this refers to the element that received the event.
Methods like call(), apply(), and bind() can refer this to any object. 

Note
this is not a variable. It is a keyword. You cannot change the value of this.
*/

class Vehicle {
    constructor(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }

    start() {
        console.log('Starting the vehicle');
    }

    stop() {
        console.log('Stopping the vehicle');
    }

    honk() {
        console.log('Honk!');
    }
};

class Car extends Vehicle {
    constructor(make, model, year) {
        super(make, model, year);
    };

    start() {
        super.start();
        console.log('Starting the car engine');
    };
    stop() {
        super.stop();
        console.log('Stopping the car engine');
    };
};

class Truck extends Vehicle {
    constructor(make, model, year) {
        super(make, model, year);
    };
    start() {
        super.start();
        console.log('Starting the truck engine');
    };
};

class Motorcycle extends Vehicle {
    constructor(make, model, year) {
        super(make, model, year);
    };
    honk() {
        console.log('Beep beep!');
    };
};

// Showing the usage of the parent class and proto property in the child class
class Bicycle extends Vehicle {
    constructor(make, model, year) {
        super(make, model, year);
    };
    // Showing the proto property
    showProto() {
        console.log("Proto: ", Object.getPrototypeOf(this));
    };
}

const bicycle = new Bicycle("Honda", "CBR 100RR", 2019);
console.log("Bicycle: ", bicycle);
bicycle.showProto();


const person1 = {
    fullName: function () {
        return this.firstName + " " + this.lastName;
    }
};
const person2 = {
    firstName: "John",
    lastName: "Doe",
};
// Return "John Doe":
person1.fullName.call(person2);


export {
    Vehicle,
    Car,
    Truck,
    Motorcycle,
    bicycle
}
