"use strict";

console.log("Hello World");

let array1 = [1, 2, 3, 4, 5];
console.log(array1);
let array2 = [6, 7, 8, 9, 10];
let array3 = [...array1, ...array2];
// console.log(`Current Array3 is ${[...array3]} `);
console.log(array3);

// let obj1 = { a: 1, b: 2, c: 3 };
// let obj2 = { d: 4, e: 5, f: 6 };
// let obj3 = { ...obj1, ...obj2 };
// console.log(obj3)
// 
// let obj4 = { a: 1, b: 2, c: 3 };
// let obj5 = { d: 4, e: 5, f: 6 };
// let obj6 = { ...obj4, ...obj5 };
// console.log(obj6)

let array4 = array3.splice(0, 10);
console.log(array4);
console.log(array3);

function loadButton() {
    const button = document.getElementById('btn');
    const output = document.getElementById('output');
    if (button) {
        button.addEventListener('click', function () {
            const message = "Button clicked!";
            console.log(message);
            output.innerText = message;
            output.classList.remove('fade-out'); // Reset fade-out effect
            // showToast(message);
            // Trigger the fade-out effect after 2 seconds
            setTimeout(function () {
                output.classList.add('fade-out');
            }, 2000);
        });
    } else {
        console.error('Button element not found');
    }
};



export {
    loadButton
};